<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@dashboard');

Route::get('/data-table', function(){
    return view('table.data-table');
});

Route::get('/table', function(){
    return view('table.table');
});

//Crud

Route::get('/film/create', 'FilmController@create');
Route::post('/film', 'FilmController@store');
Route::get('/film', 'FilmController@index');
Route::get('/film/{film_id}', 'FilmController@show');
Route::get('/film/{film_id}/edit', 'FilmController@edit');
Route::put('film/{film_id}', 'FilmController@update');
Route::delete('film/{film_id}', 'FilmController@destroy');


// CRUD CAST
Route::resource('cast', 'CastController');