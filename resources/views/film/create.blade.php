@extends('layout.master')

@section('judul')
    Halaman Create film
@endsection

@section('content')
    
    <form action="/film" method="POST">
    @csrf
    <div class="form-group">
        <label >Nama Judul</label>
        <input type="text" class="form-control" name="judul" >
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Ringkasan</label>
        <input type="text" class="form-control" name="ringkasan" >
        
    </div>
    @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Tahun</label>
        <input type="text" class="form-control" name="tahun" >
        
    </div>
    @error('tahun')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Poster</label>
        <input type="text" class="form-control" name="poster" >
        
    </div>
    @error('poster')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Genre Id</label>
        <input type="text" class="form-control" name="genre_id" >
        
    </div>
    @error('genre_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection

   
