@extends('layout.master')

@section('judul')
    Halaman Edit Cast
@endsection

@section('content')
    
    <form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label >Nama Cast</label>
        <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Umur</label>
        <input type="text" class="form-control" name="umur" value="{{$cast->umur}}">
        
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Bio</label>
        <textarea type="text" class="form-control" name="bio" cols="30" rows="10" >{{$cast->bio}}</textarea>
        
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection

   
