@extends('layout.master')

@section('judul')
    Halaman List Cast
@endsection

@section('content')
    
<a href="/cast/create" class="btn btn-success mb-3">Tambah</a>

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>                
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $item)
                <tr>
                    <td>{{ $key + 1}}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->umur }}</td>
                    <td>{{Str::limit( $item->bio, 20, '...') }}</td>
                    <td>
                        
                        <form action="/cast/{{$item->id}}" method="POST">
                            @method('delete')
                            @csrf

                            <a href="/cast/{{$item->id}}" class="btn btn-secondary btn-sm">Detail</a>
                            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                </tr>
            @empty
                <tr>
                    <td>Data Masih Kosong</td>
                </tr>
            @endforelse
        </tbody>
    </table>

@endsection

   
    
    


